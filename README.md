# First Choice Travel Hub Repository

## FOR STRICT COMPLIANCE
### Commit Structure 

Always add TG-REF #statusCode on every commit message.
*Note that the REF came from the Taiga.io Issue Numbers*

```bash
git commit -m "TG-1 #ready-for-test"
```

Will set Issue #1 (In this case a User Story) to Ready for test status